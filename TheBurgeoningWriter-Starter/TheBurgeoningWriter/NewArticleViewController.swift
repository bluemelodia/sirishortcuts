/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import IntentsUI
import ArticleKit

class NewArticleViewController: UIViewController {
  let titleTextField = UITextField()
  let contentsTextView = UITextView()
  let addShortcutButton = UIButton()
  
  @objc func saveWasTapped() {
    if let title = titleTextField.text, let content = contentsTextView.text {
      let article = Article(title: title, content: content, published: false)
      
      ArticleManager.add(article: article)
      
      // Donate publish intent
      
      
      navigationController?.popViewController(animated: true)
    }
  }
  
  // Called when you press the "Add Shortcut to Siri" button in-app.
  @objc func addNewArticleShortcutWasTapped() {
    // Open View Controller to Create New Shortcut
    
    // Initialize the shortcut
    let newArticleActivity = Article.newArticleShortcut(thumbnail: UIImage(named: "notePad.jpg"))
    let shortcut = INShortcut(userActivity: newArticleActivity)
    
    /* IntentsUI framework provides a special VC that you can initialize with a
       shortcut. You can then present this view controller to show the same UI
       that we just saw in the Settings app. */
    let vc = INUIAddVoiceShortcutViewController(shortcut: shortcut)
    vc.delegate = self
    
    present(vc, animated: true, completion: nil)
  }
}

// Add Shortcut View Controller Delegate Extension

extension NewArticleViewController: INUIAddVoiceShortcutViewControllerDelegate {
    /* Delegate method for when user successfully creates a shortcut. */
    func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        /* Dismiss the Siri VC when the method is called. */
        dismiss(animated: true, completion: nil)
    }
    
    /* Delegate method for when the user taps the cancel button. */
    func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        dismiss(animated: true, completion: nil)
    }
}
