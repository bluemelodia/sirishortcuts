/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import Intents
import CoreSpotlight
import MobileCoreServices

// Add activity type identifier - use this to determine if you are dealing with a 'new article' shortcut.
public let kNewArticleActivityType = "siri.test.NewArticle"

public class Article {
  public let title: String
  public let content: String
  public let published: Bool
  
  // Create user activity for new articles
    public static func newArticleShortcut(thumbnail: UIImage?) -> NSUserActivity {
        /* Create and return an activity object with the correct identifier.
           The persistentIdentifier connects all of these shortcuts as one activity. */
        let activity = NSUserActivity(activityType: kNewArticleActivityType)
        activity.persistentIdentifier = NSUserActivityPersistentIdentifier(kNewArticleActivityType)
        
        // Allows users to search for this feature in Spotlight
        activity.isEligibleForSearch = true
        
        /* Allows Siri to look at the activity and suggest it to users in the future.
           Also enables the activity to be turned into a Shortcut later. */
        activity.isEligibleForPrediction = true
        
        activity.suggestedInvocationPhrase = "Time to write!"
        
        /* Sets the title, subtitle, and thumbnail image on the suggestion notification. */
        let attributes = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
        activity.title = "Write a new article"
        attributes.contentDescription = "Get those creative juices flowing!"
        attributes.thumbnailData = thumbnail?.jpegData(compressionQuality: 1.0)
        
        // Set of properties describing an activity
        activity.contentAttributeSet = attributes
        
        return activity
    }
  
  // Create an intent for publishing articles
  
  
  // MARK: - Init
  public init(title: String, content: String, published: Bool) {
    self.title = title
    self.content = content
    self.published = published
  }
  
  // MARK: - Helpers
  public func toData() -> Data? {
    let dict = ["title": title, "content": content, "published": published] as [String : Any]
    let data = try? NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
    return data
  }
  
  public func formattedDate() -> String {
    let date = Date()
    let formatter = DateFormatter()
    
    formatter.dateFormat = "MM/dd/yyyy"
    let result = formatter.string(from: date)
    
    return result
  }
}
